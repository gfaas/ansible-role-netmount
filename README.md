# ansible-role-netmount

This role simply mounts (or removes) a network share

## Requirements

- A RHEL / CentOS node as client
- An NFS or GlusterFS server with an active export

Quite likely CIFS/SMB would work as well, but this is not tested!

During the execution of the role, nfs-utils will be installed.  If your client host is using RHEL6,
then also netfs is installed (and enabled at boot time).

*Note: If you are migrating from (old) RHEL-6 based GlusterFS mounts (using a GlusterFS-fuse mount) to
a RHEL-7 based GlusterFS/NFS-Ganesha solution, you can specify the new sharename for the existing mountpoint
and it will unmount the current GlusterFS mount, and replace this with the new NFS-based mount. 
Be sure to stop all client processes that might use the (old) mount, else it can not be unmounted.*

Sample requirements.yml file to be included:

```yaml
---
# Get the netmount role from Gluster
- name: ansible-role-netmount
  src: https://gitlab.com/gfaas/ansible-role-netmount.git
  version: master
  scm: git
```

## Role Variables

Optional variables that can be set (incl. their default values)

```yaml
  netmount:
  - path: /path/on/your/local/server
    location: someserver.example.com:/exported_path
    extraopt: list of NFS options  (_netdev is by default already set)
    state: mounted | absent
```

## Sample playbook (RHEL6, NFS)

```yaml
---
- name: Mount an NFS export
  hosts: client01
  serial: 1

  roles:
    - role: ansible-role-netmount
      netmount:
      - path: /mnt
        location: server.example.com:NEW2
        extraopt:
          - rw
          - vers=4
          - minorversion=0
          - defaults
        state: mounted
        type: nfs
...
```

## Sample playbook (RHEL7, NFS)

```yaml
---
- name: Mount an NFS export
  hosts: client01
  serial: 1

  roles:
    - role: ansible-role-netmount
      netmount:
      - path: /mnt
        location: server.example.com:NEW2
        extraopt:
          - rw
          - vers=4.0
          - defaults
        state: mounted
        type: nfs
...
```

## Sample playbook (GlusterFS)

```yaml
---
- name: Mount a GlusterFS share
  hosts: client01
  serial: 1

  roles:
    - role: ansible-role-netmount
      netmount:
      - path: /data
        location: server.example.com:NEW1
        extraopt:
          - rw
          - defaults
        state: mounted
        type: glusterfs
...
```

## Sample playbook (Combinations)

```yaml
---
- name: Mount a GlusterFS share
  hosts: client01
  serial: 1

  roles:
    - role: ansible-role-netmount
      netmount:
      - path: /data/share1
        location: server.example.com:NEW1
        extraopt:
          - rw
          - defaults
        state: mounted
        type: glusterfs
      - path: /data
        location: server.example.com:NEW2
        extraopt:
          - ro
          - vers=4.1
          - defaults
        state: mounted
        type: nfs
...

```
